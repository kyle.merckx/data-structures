# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.
class LinkedQueue():
    head = None
    tail = None
    length = 0

    def dequeue(self):
        value = self.head.value
        if self.length > 1:
            self.head = self.tail
        else:
            self.head = None
            self.tail = None

        self.length -= 1
        return value

    def enqueue(self, value):
        node = LinkedQueueNode(value)
        if (self.head == None):
            self.head = node
            self.tail = node
        previous_tail = self.tail

        self.tail = node
        previous_tail.link = node

        self.length += 1

class LinkedQueueNode():
    def __init__(self, value, link=None):
        self.value = value
        self.link = link
    pass
