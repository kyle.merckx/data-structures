# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# circular_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_XX.py.


class CircularQueue():
    def __init__(self, size):
        self.size = size
        self.head = 0
        self.tail = 0
        self.buffer = [None] * self.size
        self.length = 0

    def enqueue(self, value):
        if self.tail == self.size - 1:
            self.tail = 0
        else:
            self.buffer.insert(self.tail, value)
            self.length += 1
            self.tail += 1

    def dequeue(self):
        value = self.buffer[self.head]
        if self.head == self.size - 1:
            self.head = 0
        else:
            self.length -= 1
            self.head += 1
        return value
