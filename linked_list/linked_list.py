# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

class LinkedList():
    head = None
    tail = None
    length = 0

    def insert(self, value, position=float("-inf")):
        node = LinkedListNode(value)
        if self.head == None:
            self.head = node
            self.tail = node
        elif position > 0 and position < self.length:
            current_node = self.head
            for i in range(0 , self.length):
                if i == position:
                    current_node = current_node.link
                    previous_link = current_node.link
                    node.link = previous_link
                    current_node.link = node
                if current_node != self.tail:
                    curent_node = current_node.link
        elif position == 0:
            node.link = self.head
            self.head = node
        else:
            previous_tail = self.tail
            self.tail = node
            previous_tail.link = node

        self.length += 1

    def get(self, index):
        value = 0

        if index >= self.length:
            raise IndexError
        elif index < 0:
            current_node = self.head
            for i in range(0, self.length):
                if i == self.length + index:
                    value = current_node.value
                if current_node != self.tail:
                    current_node = current_node.link
        else:
            current_node = self.head
            for i in range(0 , self.length):
                if i == index:
                    value = current_node.value
                if current_node != self.tail:
                    current_node = current_node.link
        return value

    def remove(self, index=0):
        if index == 0:
            value = self.head.value
            self.head = self.head.link
        elif index > self.length:
            raise IndexError
        elif index == self.length - 1:
            current_node = self.head
            value = self.tail.value
            for i in range(0, self.length):
                if current_node.link == self.tail:
                    current_node.link = None
                    self.tail = current_node
                if current_node != self.tail:
                    current_node = current_node.link
        else:
            previous_node = self.head
            for i in range(1, self.length):
                if i == index:
                    next_node = previous_node.link
                    value = next_node.value
                    previous_node.link = next_node.link
                if previous_node != self.tail:
                    previous_node = previous_node.link
        self.length -= 1
        return value


class LinkedListNode():
    def __init__(self, value, link=None):
        self.value = value
        self.link = link
